'use strict';
/**
 * @ngdoc directive
 * @name denguemapsApp.directive:map
 * @description
 * # slider
 */


angular.module('denguemapsApp')
  .directive('mapaRiesgos', function ($rootScope,  $http, mapServices, $q) {
    return {
		restrict: 'E',
		replace: false,
		require: '^map',
		scope: {
			detalle:'='
		},
    	link: function postLink(scope, element, attrs, mapCtrl) {
    		//mapCtrl.imprimir();
			mapCtrl.imprimir("mapaRiesgos");
			mapCtrl.cleanMap();
			
			var departamentos;
			var map = mapCtrl.getMap();
			var STATE = $rootScope.RIESGO_STATE;
			console.log("STATE::");
			console.log($rootScope.RIESGO_STATE);
			if(STATE === undefined){
				console.log("undefined::");
				STATE = {};
				STATE.inZoom = false;
				departamentos = mapCtrl.getDepartamentos();
				departamentos.then(function(data){
					STATE.geoJsonLayer = L.geoJson(data,  {onEachFeature: onEachFeature}).addTo(map);
					mapCtrl.setActualLayer(STATE.geoJsonLayer);
					return init($("#sMonth").val(), $("#sYear").val(), STATE.geoJsonLayer);
				}).then(function(data){
	          		getRiesgosLegend().addTo(map);
					getRiesgoInfo().addTo(map);
	          		data.setStyle(getStyle);
	          		$rootScope.RIESGO_STATE = STATE;
	          	});
	          	
			} else {
				STATE.geoJsonLayer.addTo(map);
				if(STATE.inZoom){
					STATE.drillDownLayer.addTo(map);
					map.fitBounds(STATE.drillDownLayer.getBounds());
					STATE.backButton.addTo(map);
					mapCtrl.addControl(STATE.backButton);
					mapCtrl.setDrillDownLayer(STATE.drillDownLayer);
				}
				STATE.info.addTo(map);
				STATE.riesgosLegend.addTo(map);
				mapCtrl.addControl(STATE.info);
				mapCtrl.addControl(STATE.riesgosLegend);
				mapCtrl.setActualLayer(STATE.geoJsonLayer);
			}

			/* Control de opacidad */
			//Create the opacity controls
	        /*var opacitySlider = new L.Control.opacitySlider();
	        map.addControl(opacitySlider);

	    	//Specify the layer for which you want to modify the opacity. Note that the setOpacityLayer() method applies to all the controls.
	    	//You only need to call it once.
	        opacitySlider.setOpacityLayer(departamentos);

	    	//Set initial opacity to 0.5 (Optional)
        	departamentos.setOpacity(0.5);*/
        	//mapUtil.setMap(map);

        	$("#sMonth").on({
				slide: function(){
					reloadSem($("#sMonth").val());
				}
			});

			$("#sYear").on({
				change: function(){
					reloadAnio($("#sMonth").val(), $("#sYear").val());
				}
			});

        	
			var backClass =  L.Control.extend({
		        options: {
		            position: 'topright'
		        },

		        onAdd: function (map) {
		            var controlDiv = L.DomUtil.create('div', 'leaflet-control-command');
		            L.DomEvent
		                .addListener(controlDiv, 'click', L.DomEvent.stopPropagation)
		                .addListener(controlDiv, 'click', L.DomEvent.preventDefault)
		            .addListener(controlDiv, 'click', drillUp);

		            var controlUI = L.DomUtil.create('div', 'leaflet-control-command-interior', controlDiv);
		            controlUI.title = 'Map Commands';
		            return controlDiv;
		        }
		    });
		    

			function init(sem, ano, layer){
	  			var defered = $q.defer();
	      		var promise = defered.promise;
	  			STATE.semana = sem;
	  			STATE.anio = ano;
	  			return mapServices.getRiesgosDepartamentos(STATE.anio)
	  				.then(function(data){
	    				STATE.departamentosRiesgo =  data;
	    				return mapServices.getRiesgosDistritos(STATE.anio);
	    			})
	  				.then(function(data){
	  					console.log("riesgos de distritos- ");
	  					STATE.distritosRiesgo = data;
	  					return mapServices.getRiesgosAsuncion(STATE.anio);
	  				})
	  				.then(function(data){
	  					console.log("riesgos de asuncion- ");
	  					STATE.asuncionRiesgo = data;
	  					reloadSem(STATE.semana);
	  					defered.resolve(layer);
	  					return promise;
	  				})
	    			.catch(function(err){
	    				console.log(err);
	    			});
	  		}

	  		function reloadAnio(sem, ano){
	  			STATE.semana = sem;
	  			STATE.anio = ano;
	  			mapServices.getRiesgosDepartamentos(STATE.anio)
	  				.then(function(data){
	    				STATE.departamentosRiesgo =  data;
	    				return mapServices.getRiesgosDistritos(STATE.anio);
	    			})
	  				.then(function(data){
	  					console.log("riesgos de distritos- ");
	  					STATE.distritosRiesgo = data;
	  					return mapServices.getRiesgosAsuncion(STATE.anio);
	  				})
	  				.then(function(data){
	  					console.log("riesgos de asuncion- ");
	  					STATE.asuncionRiesgo = data;
	  					reloadSem(STATE.semana);

	  				})
	    			.catch(function(err){
	    				console.log(err);
	    			});
	  		}

			function reloadSem(semana){
				STATE.semana = semana;
			    var rasu;
			    var rcen;
			    var mapSem = new Object();
			    var mapSemDis = new Object();

			    //Cargar el json de los riesgos por semana
			    //riesgos de todas las semanas de los departamentos
			    var riesgo = STATE.departamentosRiesgo;
			    //riesgos de todas las semana de los distritos
			    var riesgoDis = STATE.distritosRiesgo;
			    //riesgos de todas las semanas de los barrios de asuncion
			    var riesgoAsu = STATE.asuncionRiesgo;
			   	console.log("reloadSem - - -");

			    for(var i=0; i<riesgo.length; i++){
			        var obj = riesgo[i];
			        if(obj["semana"]== semana ){
			            mapSem[obj["departamento"]]= obj;
			            if(obj["departamento"]=="CENTRAL"){
			                rcen = obj;
			            }
			            if(obj["departamento"]=="ASUNCION"){
			                rasu = obj;
			            }
			        }
			    }
			    if(rasu){
			       try{
			            rasu["riesgo"] = rcen["riesgo"];
			        }catch(e){
			            rasu["riesgo"] = 'RB';
			        }
			        mapSem["ASUNCION"] = rasu;
			    }
			    //Riesgo de los departamentos de la semana seleccionada
			    STATE.riesgoSemanaDep = mapSem;

			    for(var i=0; i<riesgoDis.length; i++){
			        var obj = riesgoDis[i];
			        if(obj["semana"]== semana ){

			            mapSemDis[obj["distrito"]]= obj;
			            /*if(obj["departamento"]=="CENTRAL"){
			                rcen = obj;
			            }*/
			            if(obj["distrito"] == "ASUNCION"){
			                //rasu = obj;
			            }
			        }
			    }
			    //Incluir en el mapSemDis los barrios de Asuncion
			    for(var i=0; i<riesgoAsu.length; i++){
			        var obj = riesgoAsu[i];
			        if(obj["semana"] == semana){
			            mapSemDis[obj["barrio"]] = obj;
			        }

			    }
			   	//Riesgos de los distritos de la semana seleccionada
			    STATE.riesgoSemanaDis = mapSemDis;
			    STATE.geoJsonLayer.setStyle(getStyle);
			    if(STATE.inZoom){
			    	STATE.drillDownLayer.setStyle(getStyleDrillDown);
			    }
			}


	  		function getRiesgosLegend(){
	  			STATE.riesgosLegend = L.control({
			        position: 'bottomright'
			    });
			    STATE.riesgosLegend.onAdd = function(map) {
			        var div = L.DomUtil.create('div', 'info legend'), labels = [];
			        labels.push('<i style="background:' + getColor('E') + '"></i> ' + 'Epidemia');
			        labels.push('<i style="background:' + getColor('RA') + '"></i> ' + 'Riesgo alto');
			        labels.push('<i style="background:' + getColor('RM') + '"></i> ' + 'Riesgo medio');
			        labels.push('<i style="background:' + getColor('RB') + '"></i> ' + 'Riesgo bajo');
			        div.innerHTML = '<span>Umbrales de riesgo</span><br>' + labels.join('<br>')
			        return div;
			    };
			    mapCtrl.addControl(STATE.riesgosLegend);
			    return STATE.riesgosLegend;
	  		}

	  		function getRiesgoInfo(){
				STATE.info = L.control();
			    STATE.info.onAdd = function (map) {
			        this._div = L.DomUtil.create('div', 'info');
			        this.update();
			        return this._div;
			    };
			    STATE.info.update = function (props) {
			    	
			        if(props){

			            var dep = props.dpto_desc;
			            //if(SMV.onriesgo){
			                var mapSem = STATE.riesgoSemanaDep;
			            /*}else{
			                var mapSem = SMV.mapSemFil;
			            }*/

			            var nroNS = '0';
			            try{
			                nroNS = mapSem[dep]["cantidad"];
			            }catch(e){

			            }
			          this._div.innerHTML =  '<h2>Año: '+STATE.anio+' - Semana: '+STATE.semana+'<\/h2><h2>Dpto: '+dep+'<\/h2><h2>Notificaciones: '+nroNS+'<\/h2>';
			        }
			    };
			    STATE.info.updateDrillDown = function (props){

			        if(props){

			            var dep = props.dpto_desc;
			            var depAsu = props.dpto_desc;
			            var dis = props.dist_desc;
			            var mapSem = STATE.riesgoSemanaDis;
			            var nroNS = '0';

			            var info;
			            if(depAsu == 'ASUNCION'){
			            	dis = props.dist_desc;
			                var key = depAsu+'-'+props.barlo_desc;
			                try{
			                    nroNS = mapSem[key]["cantidad"];
			                }catch(e){

			                }
			                info = '<h2>Año: '+STATE.anio+' - Semana: '+STATE.semana+'<\/h2><h2>Dpto: '+depAsu+'<\/h2><h2>Distrito: '+dis+'<\/h2><h2>Barrio: '+props.barlo_desc+'<\/h2><h2>Notificaciones: '+nroNS+'<\/h2>';
			            } else {
			                var key = dep+'-'+dis;
			                try{
			                    nroNS = mapSem[key]["cantidad"];
			                }catch(e){

			                }
			                info = '<h2>Año: '+STATE.anio+' - Semana: '+STATE.semana+'<\/h2><h2>Dpto: '+dep+'<\/h2><h2>Distrito: '+dis+'<\/h2><h2>Notificaciones: '+nroNS+'<\/h2>';
			            }
			          this._div.innerHTML =  info;
			        }
		    	};
		    	mapCtrl.addControl(STATE.info);
		    	return STATE.info;
		    }

			function onEachFeature(feature, layer) {
			    layer.on({
			        mouseover: mouseover,
			        mouseout: mouseout,
			        click: zoomToFeature
			    });

			}

			function mouseover(e) {
			    var layer = e.target;
			     layer.setStyle({
			        weight: 5,
			        color: '#666',
			        dashArray: ''

			    });

			    if (!L.Browser.ie && !L.Browser.opera) {
			        layer.bringToFront();
			    }
			    STATE.info.update(layer.feature.properties);
			}

			/*Evento al salir el puntero de un departamento*/
			function mouseout(e) {
			    //geoJsonLayer.resetStyle(e.target);
			    var layer = e.target;
			     layer.setStyle({
			        weight: 1,
			        color: '#FFF',
			        dashArray: ''

			    });
			    STATE.info.update();

			}

			/*Zoom al hacer click en un departamento*/
			function zoomToFeature(e) {
				
			    var target = e.target;
			    //pedir distritos?
			    console.log(target.feature.properties.dpto_desc);
			    if(!STATE.inZoom){
				   	STATE.backButton = new backClass();
			        map.addControl(STATE.backButton);
			        mapCtrl.addControl(STATE.backButton);
			    }
			    if(target.feature.properties.dpto_desc == 'ASUNCION'){
			    	 mapServices.getBarrios(target.feature.properties.dpto_desc)
			    	.then(function(data){
	    				var json = data;
	    				STATE.inZoom = true;
	    				STATE.geoJsonLayer.setStyle(getStyle);
					    if(STATE.drillDownLayer){
					        map.removeLayer(STATE.drillDownLayer);
					    }
					    STATE.drillDownLayer = L.geoJson(json,  {style: getStyleDrillDown, onEachFeature: onEachFeatureDrillDown}).addTo(map);
					    map.fitBounds(target.getBounds());
					    console.log(data);
					    mapCtrl.setDrillDownLayer(STATE.drillDownLayer);
	    			});
			    } else {
			    	mapServices.getDistritos(target.feature.properties.dpto_desc)
			    	.then(function(data){
	    				var json = data;
	    				STATE.inZoom = true;
	    				STATE.geoJsonLayer.setStyle(getStyle);
					    if(STATE.drillDownLayer){
					        map.removeLayer(STATE.drillDownLayer);
					    }
					    STATE.drillDownLayer = L.geoJson(json,  {style: getStyleDrillDown, onEachFeature: onEachFeatureDrillDown}).addTo(map);
					    map.fitBounds(target.getBounds());
					    console.log(data);
					    mapCtrl.setDrillDownLayer(STATE.drillDownLayer);
					    console.log(STATE.drillDownLayer);
	    			});
			    }
			}

	    	function getColor(d) {
			    return d == 'E' ? '#FE0516' :
			        d == 'RA' ? '#FF6905' :
			        d == 'RM' ? '#FFB905' :
			        '#FFF96D';
			}

			/*Estilo de la capa de departamento de acuedo a los valores de riesgo*/
			function getStyle(feature) {
			    var n = feature.properties.dpto_desc;
			    var mapSem = STATE.riesgoSemanaDep;
			    var color = 'NONE';
			    try{
			        color = mapSem[n]["riesgo"];
			    }catch(e){
			    }
			    if(STATE.inZoom){
			    	return { weight: 2,
			            opacity: 1,
			            color: 'white',
			            dashArray: '3',
			            fillOpacity: 0,
			            fillColor: getColor(color)
			        };
			    }
			  	else{
			  		return { weight: 2,
			            opacity: 1,
			            color: 'white',
			            dashArray: '3',
			           	fillOpacity: 0.5,
			            fillColor: getColor(color)
			        };
			  	}

			}

			/*Estilo de la capa de distritos de acuedo a los valores de riesgo*/
			function getStyleDrillDown(feature) {
			    var prop = feature.properties;
			    var n = prop.dpto_desc+'-'+prop.dist_desc;
			    var mapSem = STATE.riesgoSemanaDis;
			    var color = 'NONE';
			    if(prop.dpto_desc == 'ASUNCION'){
			        n = prop.dpto_desc+'-'+prop.barlo_desc;
			    }
			   try{
			        color = mapSem[n]["riesgo"];
			       
			    }catch(e){
			    }
			   
			    return { weight: 2,
			            opacity: 1,
			            color: 'white',
			            dashArray: '3',
			            fillOpacity: 0.5, 
			            fillColor: getColor(color) 
			        };
			}
			/*Eventos para cada distrito*/
			function onEachFeatureDrillDown(feature, layer) {
			    layer.on({
			        mouseover: mouseoverDrillDown,
			        mouseout: mouseoutDrillDown,
			    });
			}
			/*Evento al ubicarse el puntero sobre un distrito*/
			function mouseoverDrillDown(e) {
			    var layer = e.target;
			     layer.setStyle({
			        weight: 5,
			        color: '#666',
			        dashArray: ''
			        
			    });

			    if (!L.Browser.ie && !L.Browser.opera) {
			        layer.bringToFront();
			    }
			    STATE.info.updateDrillDown(layer.feature.properties);
			}
			/*Evento al salir el puntero de un distrito*/
			function mouseoutDrillDown(e) {
			    STATE.drillDownLayer.resetStyle(e.target);
			    STATE.info.updateDrillDown();
			   
			}
			function drillUp(){
			    //map.fitBounds(SMV.STATE.geoJsonLayer.getBounds());
			    map.removeLayer(STATE.drillDownLayer);
			    STATE.drillDownLayer = undefined;
			   	STATE.inZoom = false;
			    STATE.geoJsonLayer.setStyle(getStyle);
			    STATE.backButton.removeFrom(map);
			    mapCtrl.removeControl(STATE.backButton);
			    map.setView([-23.388, -60.189], 7);
			}

	    }
	};
});