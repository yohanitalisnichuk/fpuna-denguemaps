'use strict';
/**
 * @ngdoc directive
 * @name denguemapsApp.directive:map
 * @description
 * # slider
 */


angular.module('denguemapsApp')
  .directive('mapaReportes', function ($rootScope,  $http, mapServices, $q) {
    return {
		restrict: 'E',
		replace: false,
		require: '^map',
		scope: {
			detalle:'='
		},
    	link: function postLink(scope, element, attrs, mapCtrl) {
    		//mapCtrl.imprimir();
			mapCtrl.imprimir("mapaReportes");
			mapCtrl.cleanMap();
			var STATE = $rootScope.CASOS_STATE;
			var map = mapCtrl.getMap();
			var departamentos = mapCtrl.getDepartamentos();
			if(STATE === undefined){
				STATE = {};
				departamentos.then(function(data){
					STATE.geoJsonLayer = L.geoJson(data,  {onEachFeature:onEachFeatureNotificaciones}).addTo(map);
					mapCtrl.setActualLayer(STATE.geoJsonLayer);
					return init($("#sMonth").val(), $("#sYear").val(), STATE.geoJsonLayer);
				}).then(function(data){
					getLegend().addTo(map);
					getInfo().addTo(map);
					data.setStyle(getStyleNotificaciones);
	        $rootScope.CASOS_STATE = STATE;
          STATE.info.update();
	      });

			} else {
				STATE.geoJsonLayer.addTo(map);
				STATE.legendNoti.addTo(map);
				STATE.info.addTo(map);
				mapCtrl.setActualLayer(STATE.geoJsonLayer);
				mapCtrl.addControl(STATE.legendNoti);
				mapCtrl.addControl(STATE.info);
        STATE.info.update();
			}

			$("#sMonth").on({
				change: function(){
					reloadNotificaciones($("#sMonth").val());
				}
			});

			$("#sYear").on({
				change: function(){
					reloadAnio($("#sMonth").val(), $("#sYear").val());
				}
			});

			function init(sem, ano, layer){
				var defered = $q.defer();
	      		var promise = defered.promise;
	  			STATE.semana = sem;
	  			STATE.anio = ano;
	  			return mapServices.getCasosDepartamentos(STATE.anio)
	  				.then(function(data){
	    				STATE.resFiltro = data;
	  					reloadNotificaciones(STATE.semana);
	  					defered.resolve(layer);
	  					return promise;
	    			})
	    			.catch(function(err){
	    				console.log(err);
	    			});
			}

			function reloadAnio(sem, ano){
				var defered = $q.defer();
	      		var promise = defered.promise;
	  			STATE.semana = sem;
	  			STATE.anio = ano;
	  			mapServices.getCasosDepartamentos(STATE.anio)
	  				.then(function(data){
	    				STATE.resFiltro = data;
	    				console.log(data);
	  					reloadNotificaciones(STATE.semana);
	    			})
	    			.catch(function(err){
	    				console.log(err);
	    			});
			}

			function reloadNotificaciones (semana) {
				STATE.semana = semana;
			    var mapSemFil = new Object();
		        for(var i=0; i<STATE.resFiltro.length; i++){
		            var obj = STATE.resFiltro[i];

		            if(obj["semana"] == semana){
		                mapSemFil[obj["departamento"]] = obj;
		            }
		        }
		        STATE.casosSemana = mapSemFil;
		        STATE.geoJsonLayer.setStyle(getStyleNotificaciones);
			}

			function getColorNotificaciones(d) {
			    return    d > 1000    ? '#FE0516' :
			            d > 500    ? '#FD6C24' :
			            d > 100    ? '#FFC56E' :
			            d > 50    ? '#FDFC58' :
			            d > 20    ? '#DBFB69' :
			            d > 10    ? '#BCFD82' :
			            d > 1    ? '#8EF435' :
			                        '#FFFFFF';
            };

            function getStyleNotificaciones (feature) {
			    var n = feature.properties.dpto_desc;
			    var mapSem = STATE.casosSemana;
			    var color = 'NONE';

			    try{
			        color = mapSem[n]["cantidad"];
			    }catch(e){
			    }

			    return { weight: 2,
			        opacity: 1,
			        color: 'white',
			        dashArray: '3',
			        fillOpacity: 0.8,
			        fillColor: getColorNotificaciones(color)
			    };
			}
    		function onEachFeatureNotificaciones(feature, layer) {
			    layer.on({
			        mouseover: mouseoverNotificaciones,
			        mouseout: mouseoutNotificaciones,
			    });
			}
			/*Evento al ubicarse el puntero sobre un distrito*/
			function mouseoverNotificaciones(e) {
			    var layer = e.target;
			     layer.setStyle({
			        weight: 5,
			        color: '#666',
			        dashArray: ''

			    });

			    if (!L.Browser.ie && !L.Browser.opera) {
			        layer.bringToFront();
			    }
			   STATE.info.update(layer.feature.properties);
			}

			/*Evento al salir el puntero de un distrito*/
			function mouseoutNotificaciones(e) {
			   // STATE.geoJsonLayer.resetStyle(e.target);
			    var layer = e.target;
			     layer.setStyle({
			        weight: 1,
			        color: '#FFF',
			        dashArray: ''

			    });
			    STATE.info.update();
			}
			function getLegend(){
				STATE.legendNoti = L.control({position: 'bottomright'});
			    STATE.legendNoti.onAdd = function (map) {
			        var div = L.DomUtil.create('div', 'info legend'),
			            grades = [1, 10, 20, 50, 100, 500, 1000/*, 200, 300, 400, 500, 600, 700*/],
			            labels = [],
			            from, to;
			        labels.push('<i style="background:' + getColorNotificaciones(0) + '"></i> ' + '0');
			        for (var i = 0; i < grades.length; i++) {
			            from = grades[i];
			            to = grades[i + 1];
			            labels.push(
			                '<i style="background:' + getColorNotificaciones(from + 1) + '"></i> ' +
			                from + (to ? '&ndash;' + to : '+'));
			        }
			        div.innerHTML = '<span>N&uacutemero de Notificaciones</span><br>' + labels.join('<br>')
			        return div;
			    };
			    mapCtrl.addControl(STATE.legendNoti);
			    return STATE.legendNoti;
			}

			function getInfo(){
				STATE.info = L.control();
			    STATE.info.onAdd = function (map) {
			        this._div = L.DomUtil.create('div', 'info');
			        this.update();
			        return this._div;
			    };
			    STATE.info.update = function (props) {

			        if(props){

			            var dep = props.dpto_desc;
		                var mapSem = STATE.casosSemana;
			            var nroNS = '0';
			            try{
			                nroNS = mapSem[dep]["cantidad"];
			            }catch(e){

			            }
			          this._div.innerHTML =  this._div.innerHTML =  '<b>Mapa de Casos Reportados</b><br/>'
			          					+ '<b>Año:</b> '+ STATE.anio
			          					+ ' - <b>Semana:</b> ' + STATE.semana
			          					+ '<br/><b>Dpto:</b> ' + dep
			          					+ '<br/><b>Notificaciones: </b>'+nroNS+ '<br/>';
			        } else {
                this._div.innerHTML = '<b>Mapa de Casos Reportados</b><br/>';
              }
			    };
			    STATE.info.updateDrillDown = function (props){

			        if(props){

			            var dep = props.first_dp_1;
			            var depAsu = props.dpto_desc;
			            var dis = props.first_di_1;
			            var mapSem = STATE.riesgoSemanaDis;
			            var nroNS = '0';

			            var info;
			            if(depAsu == 'ASUNCION'){
			            	dis = props.dist_desc;
			                var key = depAsu+'-'+props.barlo_desc;
			                try{
			                    nroNS = mapSem[key]["cantidad"];
			                }catch(e){

			                }
			                info = '<b>Mapa de Casos Reportados</b><br/>'
			                	+ '<b>Año: </b>' + STATE.anio
			                	+ ' - <b>Semana: </b>' + STATE.semana
			                	+ '<br/><b>Dpto: </b>' + depAsu
			                	+ '<br/><b>Distrito: </b>' + dis
			                	+ '<br/><b>Barrio:</b> ' + props.barlo_desc
			                	+ '<br/><b>Notificaciones:</b> '+ nroNS;
			            } else {
			                var key = dep+'-'+dis;
			                try{
			                    nroNS = mapSem[key]["cantidad"];
			                }catch(e){

			                }
			                 info = '<b>Mapa de Casos Reportados</b><br/>'
			                	+ '<b>Año: </b>' + STATE.anio
			                	+ ' - <b>Semana: </b>' + STATE.semana
			                	+ '<br/><b>Dpto: </b>' + depAsu
			                	+ '<br/><b>Distrito: </b>' + dis
			                	+ '<br/><b>Notificaciones:</b> '+ nroNS;
			            }
			          this._div.innerHTML =  info;
			        }
		    	};
		    	mapCtrl.addControl(STATE.info);
		    	return STATE.info;
		    }
        }
    }
});
