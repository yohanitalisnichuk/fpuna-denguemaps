package com.app.domain;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;

import com.app.utils.tables.annotation.AttributeDescriptor;

@Entity
public class Prediccion implements Serializable {

	private static final long serialVersionUID = 1L;

	@AttributeDescriptor(path = "id")
	@Id
	@SequenceGenerator(name = "prediccion_id_seq", schema = "public", sequenceName = "prediccion_id_seq", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.AUTO, generator = "prediccion_id_seq")
	@Column
	private int id;

	@AttributeDescriptor(path = "cantidad")
	@Column
	private Long cantidad;

	@AttributeDescriptor(path = "incidencia")
	@Column
	private Double incidencia;

	@AttributeDescriptor(path = "cantidadAnterior")
	@Column(name = "cantidad_anterior")
	private Long cantidadAnterior;

	@AttributeDescriptor(path = "cantidadAnteAnterior")
	@Column(name = "cantidad_ante_anterior")
	private Long cantidadAnteAnterior;

	@AttributeDescriptor(path = "adm1_nombre")
	@Column(name = "adm1_nombre")
	private String adm1Nombre;

	@AttributeDescriptor(path = "adm2_nombre")
	@Column(name = "adm2_nombre")
	private String adm2Nombre;

	@AttributeDescriptor(path = "adm3_nombre")
	@Column(name = "adm3_nombre")
	private String adm3Nombre;

	@AttributeDescriptor(path = "adm1_codigo")
	@Column(name = "adm1_codigo")
	private String adm1Codigo;

	@AttributeDescriptor(path = "adm2_codigo")
	@Column(name = "adm2_codigo")
	private String adm2Codigo;

	@AttributeDescriptor(path = "adm3_codigo")
	@Column(name = "adm3_codigo")
	private String adm3Codigo;

	@AttributeDescriptor(path = "fuente")
	@Column
	private String brote;

	@AttributeDescriptor(path = "broteSgte")
	@Column(name = "brote_sgte")
	private String broteSgte;

	@ManyToOne
	@JoinColumn(name = "id_conjunto_dato")
	private ConjuntoDato conjuntoDato;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public Long getCantidad() {
		return cantidad;
	}

	public void setCantidad(Long cantidad) {
		this.cantidad = cantidad;
	}

	public Double getIncidencia() {
		return incidencia;
	}

	public void setIncidencia(Double incidencia) {
		this.incidencia = incidencia;
	}

	public Long getCantidadAnterior() {
		return cantidadAnterior;
	}

	public void setCantidadAnterior(Long cantidadAnterior) {
		this.cantidadAnterior = cantidadAnterior;
	}

	public Long getCantidadAnteAnterior() {
		return cantidadAnteAnterior;
	}

	public void setCantidadAnteAnterior(Long cantidadAnteAnterior) {
		this.cantidadAnteAnterior = cantidadAnteAnterior;
	}

	public String getAdm1Nombre() {
		return adm1Nombre;
	}

	public void setAdm1Nombre(String adm1Nombre) {
		this.adm1Nombre = adm1Nombre;
	}

	public String getAdm2Nombre() {
		return adm2Nombre;
	}

	public void setAdm2Nombre(String adm2Nombre) {
		this.adm2Nombre = adm2Nombre;
	}

	public String getAdm3Nombre() {
		return adm3Nombre;
	}

	public void setAdm3Nombre(String adm3Nombre) {
		this.adm3Nombre = adm3Nombre;
	}

	public String getAdm1Codigo() {
		return adm1Codigo;
	}

	public void setAdm1Codigo(String adm1Codigo) {
		this.adm1Codigo = adm1Codigo;
	}

	public String getAdm2Codigo() {
		return adm2Codigo;
	}

	public void setAdm2Codigo(String adm2Codigo) {
		this.adm2Codigo = adm2Codigo;
	}

	public String getAdm3Codigo() {
		return adm3Codigo;
	}

	public void setAdm3Codigo(String adm3Codigo) {
		this.adm3Codigo = adm3Codigo;
	}

	public String getBrote() {
		return brote;
	}

	public void setBrote(String brote) {
		this.brote = brote;
	}

	public String getBroteSgte() {
		return broteSgte;
	}

	public void setBroteSgte(String broteSgte) {
		this.broteSgte = broteSgte;
	}

	public ConjuntoDato getConjuntoDato() {
		return conjuntoDato;
	}

	public void setConjuntoDato(ConjuntoDato conjuntoDato) {
		this.conjuntoDato = conjuntoDato;
	}

}
