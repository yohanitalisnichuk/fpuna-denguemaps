package com.app.domain;

import java.io.Serializable;

import javax.persistence.*;

import com.app.utils.tables.annotation.AttributeDescriptor;

@Entity
@Table(name="reporte", uniqueConstraints=@UniqueConstraint(columnNames={"semana", "anio", "mes", "dia", "region", "pais", "adm1_nombre",
	"adm2_nombre", "adm3_nombre", "grupo_edad", "sexo", "clasificacion_clinica", "estado_final", "serotipo", "origen"}))
public class Reporte implements Serializable {

	private static final long serialVersionUID = 1L;

	@AttributeDescriptor(path = "id")
	@Id
	@SequenceGenerator(name = "reporte_id_seq", schema = "public", sequenceName = "reporte_id_seq", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.AUTO, generator = "reporte_id_seq")
	@Column
	private int id;

	@AttributeDescriptor(path = "cantidad")
	@Column
	private Long cantidad;

	@AttributeDescriptor(path = "semana")
	@Column
	private String semana;

	@AttributeDescriptor(path = "anio")
	@Column
	private String anio;

	@AttributeDescriptor(path = "mes")
	@Column
	private Integer mes;

	@AttributeDescriptor(path = "dia")
	@Column
	private Integer dia;

	@AttributeDescriptor(path = "region")
	@Column
	private String region;

	@AttributeDescriptor(path = "pais")
	@Column
	private String pais;

	@AttributeDescriptor(path = "adm1_nombre")
	@Column(name = "adm1_nombre")
	private String adm1Nombre;

	@AttributeDescriptor(path = "adm2_nombre")
	@Column(name = "adm2_nombre")
	private String adm2Nombre;

	@AttributeDescriptor(path = "adm3_nombre")
	@Column(name = "adm3_nombre")
	private String adm3Nombre;

	@AttributeDescriptor(path = "grupo_edad")
	@Column
	private String grupo_edad;

	@AttributeDescriptor(path = "sexo")
	@Column
	private String sexo;

	@AttributeDescriptor(path = "clasificacion_clinica")
	@Column
	private String clasificacion_clinica;

	@AttributeDescriptor(path = "estado_final")
	@Column
	private String estado_final;

	@AttributeDescriptor(path = "serotipo")
	@Column
	private String serotipo;

	@AttributeDescriptor(path = "origen")
	@Column
	private String origen;
	
	@AttributeDescriptor(path = "adm1_codigo")
	@Column(name = "adm1_codigo")
	private String adm1Codigo;
	
	@AttributeDescriptor(path = "adm2_codigo")
	@Column(name = "adm2_codigo")
	private String adm2Codigo;
	
	@AttributeDescriptor(path = "adm3_codigo")
	@Column(name = "adm3_codigo")
	private String adm3Codigo;
	
	@AttributeDescriptor(path = "fuente")
	@Column
	private String fuente;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getSemana() {
		return semana;
	}

	public void setSemana(String semana) {
		this.semana = semana;
	}

	public String getAnio() {
		return anio;
	}

	public void setAnio(String anio) {
		this.anio = anio;
	}

	public Long getCantidad() {
		return cantidad;
	}

	public void setCantidad(Long cantidad) {
		this.cantidad = cantidad;
	}

	public Integer getMes() {
		return mes;
	}

	public void setMes(Integer mes) {
		this.mes = mes;
	}

	public Integer getDia() {
		return dia;
	}

	public void setDia(Integer dia) {
		this.dia = dia;
	}

	public String getRegion() {
		return region;
	}

	public void setRegion(String region) {
		this.region = region;
	}

	public String getPais() {
		return pais;
	}

	public void setPais(String pais) {
		this.pais = pais;
	}

	public String getAdm1Nombre() {
		return adm1Nombre;
	}

	public void setAdm1Nombre(String adm1Nombre) {
		this.adm1Nombre = adm1Nombre;
	}

	public String getAdm2Nombre() {
		return adm2Nombre;
	}

	public void setAdm2Nombre(String adm2Nombre) {
		this.adm2Nombre = adm2Nombre;
	}

	public String getAdm3Nombre() {
		return adm3Nombre;
	}

	public void setAdm3Nombre(String adm3Nombre) {
		this.adm3Nombre = adm3Nombre;
	}

	public String getAdm1Codigo() {
		return adm1Codigo;
	}

	public void setAdm1Codigo(String adm1Codigo) {
		this.adm1Codigo = adm1Codigo;
	}

	public String getAdm2Codigo() {
		return adm2Codigo;
	}

	public void setAdm2Codigo(String adm2Codigo) {
		this.adm2Codigo = adm2Codigo;
	}

	public String getAdm3Codigo() {
		return adm3Codigo;
	}

	public void setAdm3Codigo(String adm3Codigo) {
		this.adm3Codigo = adm3Codigo;
	}

	public String getGrupo_edad() {
		return grupo_edad;
	}

	public void setGrupo_edad(String grupo_edad) {
		this.grupo_edad = grupo_edad;
	}

	public String getSexo() {
		return sexo;
	}

	public void setSexo(String sexo) {
		this.sexo = sexo;
	}

	public String getClasificacion_clinica() {
		return clasificacion_clinica;
	}

	public void setClasificacion_clinica(String clasificacion_clinica) {
		this.clasificacion_clinica = clasificacion_clinica;
	}

	public String getEstado_final() {
		return estado_final;
	}

	public void setEstado_final(String estado_final) {
		this.estado_final = estado_final;
	}

	public String getSerotipo() {
		return serotipo;
	}

	public void setSerotipo(String serotipo) {
		this.serotipo = serotipo;
	}

	public String getOrigen() {
		return origen;
	}

	public void setOrigen(String origen) {
		this.origen = origen;
	}

	public String getFuente() {
		return fuente;
	}

	public void setFuente(String fuente) {
		this.fuente = fuente;
	}

}
