package com.app.domain;

import java.io.Serializable;
import java.math.BigInteger;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.xml.bind.annotation.XmlRootElement;

import com.app.utils.tables.annotation.AttributeDescriptor;

@Entity
public class Usuario implements Serializable {

	private static final long serialVersionUID = 1L;

	@AttributeDescriptor(path = "id")
	@Id
	@Column
	private Long id;

	@AttributeDescriptor(path = "activo")
	@Column
	private boolean activo;

	@AttributeDescriptor(path = "nombre")
	@Column
	private String nombre;

	@AttributeDescriptor(path = "apellido")
	@Column
	private String apellido;

	@AttributeDescriptor(path = "username")
	@Column
	private String username;

	@AttributeDescriptor(path = "pwd")
	@Column
	private String pwd;

	@AttributeDescriptor(path = "email")
	@Column
	private String email;

	@AttributeDescriptor(path = "telefono")
	@Column
	private String telefono;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public boolean isActivo() {
		return activo;
	}

	public void setActivo(boolean activo) {
		this.activo = activo;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getApellido() {
		return apellido;
	}

	public void setApellido(String apellido) {
		this.apellido = apellido;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPwd() {
		return pwd;
	}

	public void setPwd(String pwd) {
		this.pwd = pwd;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getTelefono() {
		return telefono;
	}

	public void setTelefono(String telefono) {
		this.telefono = telefono;
	}

}
