package com.app.persistence;

import java.util.List;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.Query;
import javax.persistence.TypedQuery;

@Stateless
public class ParametrosDAO {

	@Inject
	EntityManager em;

	public List<String> getDepartamentos() {
		Query query = em.createNamedQuery("findAllDepartamentos");
		return query.getResultList();

	}

	public List<String> getDistritos() {
		Query query = em.createNamedQuery("findAllDistritos");
		return query.getResultList();
	}

	public List<String> getBarrios() {
		Query query = em.createNamedQuery("findAllBarrios");
		return query.getResultList();
	}

}
