package com.app.service;

import java.util.logging.Logger;

import javax.faces.bean.RequestScoped;
import javax.inject.Inject;
import javax.inject.Named;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.app.domain.Usuario;
import com.app.persistence.UsuarioDAO;

@Named
@RequestScoped
@Path("/rest/auth")
public class AutenticacionRS {

	private final static Logger logger = Logger.getLogger(AutenticacionRS.class
			.getName());

	@Inject
	UsuarioDAO usuarioDao;

	@POST
	@Path("/login")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response autenticarUsuario(Usuario usuario) {
		logger.info("+++++++++++Autenticación de usuarios /rest/auth/login");
		String username = usuario.getUsername();
		String pwd = usuario.getPwd();
		Usuario user = usuarioDao.getUsuario(username, pwd);
		return Response.ok(user).build();
	}

}
