package com.app.service;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.logging.Logger;

import javax.faces.bean.RequestScoped;
import javax.inject.Inject;
import javax.inject.Named;
import javax.ws.rs.GET;
import javax.ws.rs.HeaderParam;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;

import com.app.constant.Statics;
import com.app.tables.facade.ReporteTableFacade;
import com.app.utils.NativeQueryFileManager;

@Named
@RequestScoped
@Path("/rest/reporte")
public class ReporteRS {

	private final static Logger logger = Logger.getLogger(ReporteRS.class
			.getName());

	@Inject
	ReporteTableFacade facade;

	@Inject
	private NativeQueryFileManager n;

	@GET
	@Path("/lista")
	// @Produces("application/json")
	public Response getCasos(@Context UriInfo uriInfo,
			@HeaderParam("Accept") String acceptHeader) {
		logger.info("++++++++++++++++++++ acceptHeader " + acceptHeader);
		MultivaluedMap<String, String> parameters = uriInfo
				.getQueryParameters();
		Map<String, String[]> finalMap = new HashMap<String, String[]>();
		Set<String> keys = parameters.keySet();
		for (String key : keys) {
			List<String> list = parameters.get(key);
			String[] a = new String[] {};
			finalMap.put(key, (String[]) list.toArray(a));
		}
		if (acceptHeader.compareTo("application/csv") == 0) {
			return Response.status(200)
					.entity(facade.getAllFilteredEntities(finalMap)).build();
		} else {
			return Response.status(200).entity(facade.getResult(finalMap))
					.build();
		}

	}

	@GET
	@Path("/descarga/{anio}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getJsonReportePorAnio(@PathParam("anio") String anio) {
		logger.info("+++++++++++++++++ Servicio de reporte por año");
		List<Map<String, Object>> list = new ArrayList<Map<String, Object>>();
		try {
			list = n.executeNativeQueryParameters(Statics.REPORTE_POR_ANIO, anio);

		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		}
		return Response.ok(list).build();
	}

}
