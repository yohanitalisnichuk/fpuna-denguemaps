package com.app.utils.tables.facade;

import java.util.Map;

import org.codehaus.jackson.node.ObjectNode;

/**
 * Created by rparra on 23/3/15.
 */
public interface DatatablesFacade<T> {
    ObjectNode getResult(Map<String,String[]> queryString);
    String getAllFilteredEntities(Map<String,String[]> queryString);

}
