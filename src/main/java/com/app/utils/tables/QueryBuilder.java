package com.app.utils.tables;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

import com.app.utils.tables.annotation.TableDescriptor;
import com.app.utils.tables.filter.BaseFilter;

/**
 * Created by rparra on 23/3/15.
 * 
 * @param <T>
 */
public class QueryBuilder<T> {

	private List<List<BaseFilter<?>>> filters = new ArrayList<>();
    private List<String> columns = new ArrayList<>();
    private Integer size;
	private Integer offset;

	final Class<T> typeParameterClass;
    protected static Logger log = Logger.getLogger(QueryBuilder.class.getName());


    public QueryBuilder addFilters(List<BaseFilter<?>> filters) {
        this.filters.add(filters);
        return this;
    }

    public QueryBuilder addAllFilters(List<List<BaseFilter<?>>> filters) {
        this.filters.addAll(filters);
        return this;
    }

	public QueryBuilder setPageSize(Integer size) {
		this.size = size;
		return this;
	}

	public QueryBuilder setOffset(Integer offset) {
		this.offset = offset;
		return this;
	}

    public QueryBuilder addColumn(String column) {
        this.columns.add(column);
        return this;
    }

    public QueryBuilder addColumns(List<String> columns) {
        this.columns.addAll(columns);
        return this;
    }

	public QueryBuilder(Class<T> typeParameterClass) {
		this.typeParameterClass = typeParameterClass;
	}

	public String build() {

		String columnList = getColumnList();

		String query = buildBaseQuery(columnList, false);
		
		/* Agregar limit y offset si tiene */
		if (size != null) {
			query += " LIMIT " + size;
		}
		if (offset != null) {
			query += " OFFSET " + offset;
		}

		return query;
	}

    public String buildSelectAll(){
        String columnList = "*";
        return buildBaseQuery(columnList, false);
    }

    public String buildTotalCount(){
        String table = getTable();
        return "SELECT count(*) FROM " + table;
    }

    public String buildCount(){
        String columnList = "count(*)";
        return buildBaseQuery(columnList, true);
    }

    private String buildBaseQuery(String columnList, Boolean isCount){
        String table = getTable();
        String query = "SELECT " + columnList + " FROM " + table;
        String wheres = "";
        String orders = "";
        String tail =  ") AND ";

		/* Obtener y agregar los filtros y ordenes al query */
        if (!filters.isEmpty()) {

            for(List<BaseFilter<?>> filtersOr: filters) {
                int count = 0;
                int countFilters = filtersOr.size();
                String subWhere = "";
                wheres += "";
                for (BaseFilter filter : filtersOr) {
                    /* Carga de condiciones where */
                    count++;
                    if(!filter.getCondition().isEmpty()){
                        if (count < countFilters) {
                            subWhere += filter.getCondition() + " OR ";
                        } else {
                            subWhere += filter.getCondition();
                        }
                    }

                /* Carga de parametro de ordenamiento */
                    if (filter.getSorting().compareTo("") != 0) {
                        orders += filter.getSorting();
                    }
                }
                if(!subWhere.isEmpty()){
                    wheres += "(" + subWhere + tail;
                }
            }

        }

        if (!wheres.isEmpty()) {
            wheres = wheres.substring(0, wheres.length() - (tail.length() - 1));
            query += " WHERE ";
            query += wheres;
        }

        if (!(orders.isEmpty() || isCount)) {
            query += " ORDER BY ";
            query += orders;
        }
        return query;
    }

    private String getColumnList(){
        String columnList = "";
        for(String column: columns){
            columnList += column + ", ";
        }
        if(!columnList.isEmpty()){
            columnList = columnList.substring(0, columnList.length() - 2);
        }
        return columnList;
    }

    private String getTable(){
        /* Obtener el nombre de la clase, define el nombre de la tabla, se pasa de CamelCase a camel_case */
        Class<?> clazz = this.typeParameterClass;
        String table = clazz.getSimpleName().replaceAll("(.)(\\p{Upper})", "$1_$2").toLowerCase();

        /* Si esta anotado, se considera la anotacion (convencion sobre configuracion) */
        if(clazz.isAnnotationPresent(TableDescriptor.class)){
            TableDescriptor descriptor = clazz.getAnnotation(TableDescriptor.class);
            if(descriptor.name() != null){
                table = descriptor.name();
            }
            if(descriptor.schema() != null){
                table = descriptor.schema() + "." + table;
            }
        }
        return table;
    }

}
